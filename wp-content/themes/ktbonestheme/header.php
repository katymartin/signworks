<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html <?php language_attributes(); ?> class="no-js">
<!--<![endif]-->



<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<meta http-equiv="content-language" content="nl">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">


	<link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
	<link href="https://fonts.googleapis.com/css?family=Lora|Open+Sans:400,600,700" rel="stylesheet">

	<link rel="canonical" href="http://signworkswi.com" />
					<meta property="og:url" content="http://signworkswi.com"/>
					<meta property="og:title" content="SignWorks Wi" />
					<meta property="og:description" content="Signworks is a full-service sign company assisting businesses nationwide with the design, production, installation and maintenance of their signs." />
					<meta property="og:type" content="article" />
					<meta property="og:image" content="http://signworkswi.com/wp-content/themes/KTBonesTheme/library/images/signworksFBpreview.png" />
					<meta property="og:image:width" content="1200" />
					<meta property="og:image:height" content="628" />

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-39015905-29"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-39015905-29');
</script>



	<title>
		<?php wp_title(''); ?>
	</title>

	<?php wp_head(); ?>






	<header class="deskNav">
		<div class="red"></div>
		<a href="/" target="_self"><img class="navLogo" src="/wp-content/themes/KTBonesTheme/library/images/signWorks_logo.png"></a>




		<div class="subNav">
			<ul>
				<li>
					<a href="tel:2626737318" target="_blank">
						<h4><i class="fa fa-phone" aria-hidden="true"></i>262.673.7318</h4>
					</a>
				</li>
				<li>
					<a href="mailto:sales@signworkswi.com" target="_blank">
						<h4><i class="fa fa-envelope" aria-hidden="true"></i>sales@signworkswi.com</h4>
					</a>
				</li>
				<li><a href="https://www.facebook.com/signworkswi/" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
				<li><a href="https://www.instagram.com/signworkswi/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
			</ul>
		</div>





		<div class="flexSec mainNav">
			<div class="flexCont flexEnd">

				<div class="flexDiv nav-item-dropdown" id="black">
					<a href="/services/" class="dropdown-trigger" target="_self">
						<h2>SERVICES<i class="arrow fa fa-arrow-down" aria-hidden="true"></i></h2>
					</a>
					<div class="dropdown-menu">
						<div class="dropdown-menu-item">
							<a href="/services-home/interior-signs/">
								<h2>INTERIOR SIGNS</h2>
							</a>
						</div>
						<div class="dropdown-menu-item">
							<a href="/services-home/exterior-signs/">
								<h2>EXTERIOR SIGNS</h2>
							</a>
						</div>
						<div class="dropdown-menu-item">
							<a href="/services-home/vehicle-graphics/">
								<h2>VEHICLE GRAPHICS</h2>
							</a>
						</div>
						<div class="dropdown-menu-item">
							<a href="/services-home/trade-show/">
								<h2>TRADE SHOW</h2>
							</a>
						</div>
						<div class="dropdown-menu-item more">
							<a href="http://signworkswi.moregreatproducts.com/Products/Products.html" target="_blank">
								<h2>MORE GREAT PRODUCTS</h2>
							</a>
						</div>
					</div>
				</div>


				<div class="flexDiv under" id="black">
					<a href="/about/" target="_self">
						<h2>ABOUT</h2>
					</a>
				</div>
				<div class="flexDiv under" id="black">
					<a href="/gallery/" target="_self">
						<h2>GALLERY</h2>
					</a>
				</div>
				<div class="flexDiv under" id="black">
					<a href="/contact/" target="_self">
						<h2>CONTACT</h2>
					</a>
				</div>
			</div>
		</div>



	</header>



	<nav class="mobileNav">
		<a href="/" target="_self"><img class="navLogo" src="/wp-content/themes/KTBonesTheme/library/images/signWorks_logo.png"></a>
		<div class="navbar"></div>
		<div class="circle"></div>
		<div class="menu">
			<ul class="topMobile">
				<li>
					<a href="/services/" target="_self">
						<h2>SERVICES</h2>
					</a>
				</li>
				<li>
					<a href="/about/" target="_self">
						<h2>ABOUT</h2>
					</a>
				</li>
				<li>
					<a href="/gallery/" target="_self">
						<h2>GALLERY</h2>
					</a>
				</li>
				<li>
					<a href="/contact/" target="_self">
						<h2>CONTACT</h2>
					</a>
				</li>
			</ul>
			<ul>
				<li class="mobileIcon">
					<a href="tel:2626737318">
						<i class="fa fa-phone" aria-hidden="true"></i>
					</a>
					<a href="mailto:sales@signworkswi.com">
						<i class="fa fa-envelope" aria-hidden="true"></i>
					</a>
					<a href="https://www.facebook.com/signworkswi/">
						<i class="fa fa-facebook-square" aria-hidden="true"></i>
					</a>
					<a href="https://www.instagram.com/signworkswi/">
						<i class="fa fa-instagram" aria-hidden="true"></i>
					</a>
				</li>
			</ul>
			</ul>
		</div>
		<div class="bars">
			<div class="x"></div>
			<div class="y"></div>
			<div class="z"></div>
		</div>
		</div>
	</nav>

</head>
