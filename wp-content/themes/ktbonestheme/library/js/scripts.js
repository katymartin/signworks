/*
 * Bones Scripts File
 * Author: Eddie Machado
 *
 * This file should contain any js scripts you want to add to the site.
 * Instead of calling it in the header or throwing it inside wp_head()
 * this file will be called automatically in the footer so as not to
 * slow the page load.
 *
 * There are a lot of example functions and tools in here. If you don't
 * need any of it, just remove it. They are meant to be helpers and are
 * not required. It's your world baby, you can do whatever you want.
*/



$("p").each(function() {
  var wordArray = $(this).text().split(" ");
  if (wordArray.length > 1) {
    wordArray[wordArray.length-2] += "&nbsp;" + wordArray[wordArray.length-1];
    wordArray.pop();
    $(this).html(wordArray.join(" "));
  }
});




/****************************************************************************
 **************************************************************************** 

 								MOBILE NAV
 
 ****************************************************************************
 ****************************************************************************/

	if ('ontouchstart' in window) {
	  var click = 'touchstart';
	} else {
	  var click = 'click';
	}

	$('div.bars').on(click, function() {

	  if (!$(this).hasClass('open')) {
	    openMenu();
	  } else {
	    closeMenu();
	  }

	});



	function openMenu() {

	  $('div.circle').addClass('expand');

	  $('div.bars').addClass('open');
	  $('div.x, div.y, div.z').addClass('collapse');
	  $('.menu li').addClass('animate');

	  setTimeout(function() {
	    $('div.y').hide();
	    $('div.x').addClass('rotate30');
	    $('div.z').addClass('rotate150');
	  }, 70);
	  setTimeout(function() {
	    $('div.x').addClass('rotate45');
	    $('div.z').addClass('rotate135');
	  }, 120);

	}

	function closeMenu() {

	  $('div.bars').removeClass('open');
	  $('div.x').removeClass('rotate45').addClass('rotate30');
	  $('div.z').removeClass('rotate135').addClass('rotate150');
	  $('div.circle').removeClass('expand');
	  $('.menu li').removeClass('animate');

	  setTimeout(function() {
	    $('div.x').removeClass('rotate30');
	    $('div.z').removeClass('rotate150');
	  }, 50);
	  setTimeout(function() {
	    $('div.y').show();
	    $('div.x, div.y, div.z').removeClass('collapse');
	  }, 70);

	}





/*
 * Get Viewport Dimensions
 * returns object with viewport dimensions to match css in width and height properties
 * ( source: http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript )
*/
function updateViewportDimensions() {
	var w=window,d=document,e=d.documentElement,g=d.getElementsByTagName('body')[0],x=w.innerWidth||e.clientWidth||g.clientWidth,y=w.innerHeight||e.clientHeight||g.clientHeight;
	return { width:x,height:y };
}
// setting the viewport width
var viewport = updateViewportDimensions();

function loadGravatars() {
  // set the viewport using the function above
  viewport = updateViewportDimensions();
  // if the viewport is tablet or larger, we load in the gravatars
  if (viewport.width >= 768) {
  jQuery('.comment img[data-gravatar]').each(function(){
    jQuery(this).attr('src',jQuery(this).attr('data-gravatar'));
  });
	}
} // end function


/*
 * Put all your regular jQuery in here.
*/
jQuery(document).ready(function($) {

$("div[class*=s-slider]").unslider({
			nav: true,
			speed: 300,
			arrows: {
			//  Unslider default behaviour
			prev: '<a class="fa unslider-arrow prev">&#xf104;</a>',
			next: '<a class="fa unslider-arrow next">&#xf105;</a>',
		},
});


}); 



jQuery(document).ready(function($) {

$("div[class*=home-slider]").unslider({
			nav: true,
			speed: 500,
			delay: 4000,
			autoplay: true,
			keys: true,
			arrows: {
			//  Unslider default behaviour
			prev: '<a class="fa unslider-arrow prev">&#xf104;</a>',
			next: '<a class="fa unslider-arrow next">&#xf105;</a>',
		},
});


}); 


jQuery(document).ready(function($) {

$("div[class*=testimonial]").unslider({
			nav: true,
			speed: 500,
			delay: 8000,
			autoplay: true,
			keys: true,
			arrows: {
			//  Unslider default behaviour
			prev: '<a class="fa unslider-arrow prev">&#xf104;</a>',
			next: '<a class="fa unslider-arrow next">&#xf105;</a>',
		},
});


}); 









/* end of as page load scripts */











/* end of as page load scripts */
