<?php
/**
 Template Name: Contact
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>

	<title>Contact | Signworks Milwaukee</title>
	<meta name="description" content="SignWorks Milwaukee contact information.">



<section class="flexSec texture" style="background: url('<?php the_field('feat_texture'); ?>') ;">
<div class="wrapper">
		<div class="pageTitle">
			<h1><?php the_field('page_title'); ?></h1>
			<hr/>
			<div class="pageAbout"><h5 style="text-align: center;"><?php the_field('page_about'); ?></h5></div>
		</div>

		<div class="flexCont">
			<?php if( have_rows('contact_info') ): ?>
			 <?php while( have_rows('contact_info') ): the_row(); ?>

			 <div class="flexDiv contact_block">

					<h2 class="testimonial-author"><?php the_sub_field('staff_member'); ?></h2>
					<h4 class="testimonial-author"><?php the_sub_field('staff_title'); ?></h4>
					<div class="con2"><img src="<?php the_sub_field('phone_ico'); ?>"><p><?php the_sub_field('staff_phone'); ?></p></div>
					<div class="con2"><img src="<?php the_sub_field('email_ico'); ?>"><p><?php the_sub_field('staff_email'); ?></p></div>

	  		</div>

			<?php endwhile; ?>
 			<?php endif; ?>
		</div>
</div>
</section>



<div ><?php include 'map.php';?> </div>


</body>



<?php get_footer(); ?>
