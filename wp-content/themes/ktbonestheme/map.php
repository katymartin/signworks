<!DOCTYPE html>
<html>
    <head>
    <body>
       <div class="mapInfo">
       		<div class="info">
       		
       		<h2>Signworks Milwaukee</h2>
       		<h4>ADDRESS</h4>
			<p>501 Sumner St</p>
      		<p>Hartford, Wi 530327</p>
       		<h4>HOURS</h4>
       		<p>Monday - Thursday: 7am - 4pm</p>
			<p>Friday: 7am - 3pm</p>
       		</div>
     
       		<a href="https://www.google.com/maps/place/Signworks/@43.3175499,-88.3881949,17z/data=!3m1!4b1!4m5!3m4!1s0x88044f5f5cd70c35:0xf05d0582bd4850cb!8m2!3d43.317546!4d-88.3860009" target="_blank"><button>GET DIRECTIONS</button></a>
       </div>
       
       <div class="mapHeight">
       <div id="map"></div>
    
    
          
     
       
        
        <script type="text/javascript">
            // When the window has finished loading create our google map below
            
			google.maps.event.addDomListener(window, 'load', init);
        	function initMap() {
				
        	var uluru = {lat: 43.3175499, lng: -88.3881949};
        	var map = new google.maps.Map(document.getElementById('map'), {
          	zoom: 4,
          	center: uluru
        	});
        	
	
				var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 11,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(43.3175499, -88.3881949), // New York
					
				
                    // How you would like to style the map. 
                    // This is where you would paste any style found on Snazzy Maps.
                     styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},
					{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
                };
				
                // Get the HTML DOM element that will contain your map 
                // We are using a div with id="map" seen below in the <body>
				
                var mapElement = document.getElementById('map');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(43.3175499, -88.3881949),
                    map: map,
                    title: 'Snazzy!'
                });
            }
        </script>
        </div></div>
        <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyByQezdv2K8KtJh9Rdh1NSnfVdsWXX_rsg&callback=initMap">
    </script>
     </body>
    </head>
    
   

        
        
   
</html>
