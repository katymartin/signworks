<?php
/**
 Template Name: Services
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

	<?php get_header(); ?><head>
	<title>Services | Signworks Milwaukee</title>
	<meta name="description" content="SignWorks Milwaukee - interior signs, trade show signs, car wraps, exterior signs">
</head>





<section class="serviceSec texture" style="background: url('<?php the_field('feat_texture'); ?>') ;">
		<div class="wrapper">

			<div class="serviceT ">
					<div class="serviceCopy">

						<h1>
							<img src="<?php the_field('service_ico'); ?>"><?php the_field('service_title'); ?>
						</h1>
						<h5><?php the_field('service_about'); ?></h5>
					</div>
				</div>
			</div>
	</section>



	<section class="serviceSec subservSec">
		<div class="wrapper">

			<div class="flexSec ">
				<div class="flexCont serv_gallery">

					<?php if( have_rows('gallery-repeater') ): ?>

					<?php while ( have_rows('gallery-repeater') ) : the_row(); ?>



					<?php

     $images = get_sub_field('gallery-images');

     if( $images ):?>
					<div class="galleryItem" id="flex-item ">
						<div class="s-slider">
							<ul>

								<?php foreach( $images as $image ): ?>
								<li style="background:url('<?php echo $image['sizes']['large']; ?>');width: auto; background-size: cover !important; background-position: center center !important; height: 350px;" ></li>
								<?php endforeach; ?>

							</ul>
						</div>
						<div class="service_subcat">
							<h2><?php the_sub_field('gallery-name'); ?></h2>
						</div>
					</div>



					<?php endif;
   endwhile;
else :
// no rows found
endif; ?>


				</div>


			</div>
		</div>

	</section>






	<?php get_footer(); ?>
