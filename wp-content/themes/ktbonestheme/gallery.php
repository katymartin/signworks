<?php
/**
 Template Name: Gallery
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>

	<title>Gallery | Signworks Milwaukee</title>
	<meta name="description" content="Image gallery">



<section class="about texture" style="background: url('<?php the_field('feat_texture'); ?>') ;">
<div class="wrapper">


		<div class="pageTitle">
			<h1><?php the_field('page_title'); ?></h1>
			<hr/>
			<div class="pageAbout"><h5 style="text-align: center;"><?php the_field('page_about'); ?></h5></div>
		</div>

	</div>
</section>




<section class="galleryWrap">

<div class="m-p-g">
	<div class="m-p-g__thumbs" data-google-image-layout data-max-height="350">
		<?php if( have_rows('gallery') ): ?>
			 <?php while( have_rows('gallery') ): the_row(); ?>
			 	<img src="<?php the_sub_field('gallery_photo'); ?>" data-full="<?php the_sub_field('gallery_photo'); ?>" class="m-p-g__thumbs-img" />
			<?php endwhile; ?>
 			<?php endif; ?>

	<div class="m-p-g__fullscreen"></div>
</div>

</div>
</section>

<script>
	var elem = document.querySelector('.m-p-g');

	document.addEventListener('DOMContentLoaded', function() {
		var gallery = new MaterialPhotoGallery(elem);
	});
</script>
  <script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/45226/material-photo-gallery.min.js"></script>


</body>


<?php get_footer(); ?>
