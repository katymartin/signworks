<footer class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">

	<div id="inner-footer" class="wrap cf">

		<div class="contactFoot secCont">
			<div class="wrapper">
				<h1>Get a Quote</h1>
				<div class="flexSec">
					<div class="flexCont">
						<div class="flexDiv flexTwo">


							<table style="width:100%">

								<tr>
									<td class="faTab"><a href="tel:2626737318" target="_blank"><i class="fa fa-phone" aria-hidden="true"></i></a></td>

									<td class="link">
										<a href="tel:2626737318" target="_blank">
											<p>262.673.7318</p>
										</a>
									</td>
								</tr>

								<tr>
									<td class="faTab"><a href="mailto:sales@signworkswi.com" target="_blank"><i class="fa fa-envelope" aria-hidden="true"></i></a></td>

									<td>
										<a href="mailto:sales@signworkswi.com" target="_blank">
											<p>sales@signworkswi.com</p>
										</a>
									</td>
								</tr>

								<tr>
									<td class="faTab"><a href="https://www.google.com/maps/place/501+W+Sumner+St,+Hartford,+WI+53027/@43.317574,-88.3881612,17z/data=!3m1!4b1!4m5!3m4!1s0x88044f5f5d23e963:0x3883165fc5228df7!8m2!3d43.317574!4d-88.3859672" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></td>

									<td>
										<a href="https://www.google.com/maps/place/501+W+Sumner+St,+Hartford,+WI+53027/@43.317574,-88.3881612,17z/data=!3m1!4b1!4m5!3m4!1s0x88044f5f5d23e963:0x3883165fc5228df7!8m2!3d43.317574!4d-88.3859672" target="_blank">
											<p>501 W Sumner St,</p>
											<p>Hartford, Wi 53207</p>
										</a>
									</td>
								</tr>

								<tr>
									<td class="faTab"><i class="fa fa-clock-o" aria-hidden="true"></i></td>
									<td>
										<p>Monday - Thursday: 7am - 4pm</p>
										<p>Friday: 7am - 3pm</p>
									</td>
								</tr>

							</table>
						</div>

						<div class="flexDiv flexTwo">
							<script type="text/javascript" src="https://form.jotform.us/jsform/72744916284162"></script>
						</div>
					</div>

				</div>
				<p class="copyRight">&copy;
					<?php echo date(Y);?> Signworks Wisconsin </p>
			</div>



		</div>

	</div>

</footer>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src='/wp-content/themes/KTBonesTheme/library/js/unslider-min.js'></script>
<script src='/wp-content/themes/KTBonesTheme/library/js/dragscroll.js'></script>


<?php // all js scripts are loaded in library/bones.php ?>
<?php wp_footer(); ?>

</body>

</html>
<!-- end of site. what a ride! -->
