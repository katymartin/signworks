<?php
/**
 Template Name: Home
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>
<head>
	<title>SignWorks Wisconsin</title>
	<meta name="description" content="SignWorks is a full-service sign company assisting businesses nationwide with the design, production, installation and maintenance of their signs. ">
</head>

<body>



<section class="homeSlide">

 <div class="home-slider"  >

	<ul>

       <?php if( have_rows('home_flick') ): ?>
 		<?php while( have_rows('home_flick') ): the_row(); ?>

        <li class="slideImg" style="background: url('<?php the_sub_field('feature_img'); ?>') ;">
        <div class="slideContent">
        <h1><?php the_sub_field('feature_title'); ?></h1>
         <p><?php the_sub_field('feature_copy'); ?></p>
        <a href="<?php the_sub_field('feature_url'); ?>" target="_self">
        <div class="headerCta"><?php the_sub_field('feature_cta'); ?></div></a>
        </div>
        </li>

        <?php endwhile; ?>
	   <?php endif; ?>

      </ul>
    </div>

</section>




<section class="customerFeature">
<div class="wrapper">
		<div class="flexSec">
			<div class="flexCont">
			<?php if( have_rows('logo_feat') ): ?>
			 <?php while( have_rows('logo_feat') ): the_row(); ?>
				<div class="flexDiv "><img src="<?php the_sub_field('logo'); ?>" alt="<?php the_sub_field('logo_alt'); ?>"></div>
			<?php endwhile; ?>
 			<?php endif; ?>
			</div>
		</div>
		<h1><?php the_field('cust_feat_copy'); ?></h1>
		<hr/>
	</div>

</section>


<section class="servicesFeature texture" style="background: url('<?php the_field('feat_texture'); ?>') ;">
<div class="wrapper">
		<div class="flexSec ">
			<div class="flexCont">
			<?php if( have_rows('serv_feat') ): ?>
			 <?php while( have_rows('serv_feat') ): the_row(); ?>

				<div class="flexDiv ">
					<a href="<?php the_sub_field('serv_feat_url'); ?>" target="_self"><img src="<?php the_sub_field('serv_ico'); ?>" alt="<?php the_sub_field('serv_ico_alt'); ?>"></a>
					<h2><?php the_sub_field('service_icon_copy'); ?></h2>
				</div>


			<?php endwhile; ?>
 			<?php endif; ?>
			</div>
		</div>

	</div>
</section>


<section class="callOut">
<div class="wrapper">
		<div class="flexSec ">
			<div class="flexCont">
			<?php if( have_rows('callout_feat') ): ?>
			 <?php while( have_rows('callout_feat') ): the_row(); ?>

				<div class="flexDiv flexThree">
					<img src="<?php the_sub_field('callout_ico'); ?>">
					<h1><?php the_sub_field('callout_head'); ?></h1>
					<p><?php the_sub_field('callout_copy'); ?></p>
				</div>


			<?php endwhile; ?>
 			<?php endif; ?>
			</div>
		</div>

	</div>
	<div class="flexSec calloutButtons">
			<div class="flexCont">
				<div class="flexDiv">
					<a href="<?php the_field('callout_btn1_url'); ?>"><button class="buttonLeft"><?php the_field('callout_btn1'); ?></button></a>
				</div>
				<div class="flexDiv">
					<a href="<?php the_field('callout_btn2_url'); ?>"><button class="buttonRight"><?php the_field('callout_btn2'); ?></button></a>
				</div>
			</div>
		</div>
</section>





<section class="testimonialsFlick">
<div class="wrapper">
	<div class="main-gallery">



			 <div class="gallery-cell">
			 	<div class="gallery-wrap">
					<div class="testimonial dragscroll">
						<ul>
						<?php if( have_rows('testimonials') ): ?>
			 <?php while( have_rows('testimonials') ): the_row(); ?>
						<li>
						<img class="testimonial-avatar" src="<?php the_sub_field('quote_ico'); ?>">
						<q class="testimonial-quote"><?php the_sub_field('quote_copy'); ?></q>
						<h2 class="testimonial-author"><?php the_sub_field('quote_author'); ?></h2>
						</li>
							<?php endwhile; ?>
 			<?php endif; ?>
						</ul>
					</div>
				</div>
	  		</div>




	</div>
</div>
</section>




</body>



<?php get_footer(); ?>
