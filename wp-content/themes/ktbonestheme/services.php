<?php
/**
 Template Name: Services Home
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>

<script>



</script>


	<title>Our Services | Signworks Milwaukee</title>
	<meta name="description" content="A breif description and portal to more information on services and offerings.">


<section class="servicesFeature texture" style="background: url('<?php the_field('feat_texture'); ?>') ;">
<div class="wrapper">


	<div class="pageTitle">
	<h1><?php the_field('page_title'); ?></h1>
	<hr/>
	<div class="pageAbout"><h5 style="text-align: center;"><?php the_field('page_about'); ?></h5></div>




	</div>
		<div class="flexSec ">
			<div class="flexCont">
			<?php if( have_rows('serv_feat') ): ?>
			 <?php while( have_rows('serv_feat') ): the_row(); ?>

				<div class="flexDiv ">
					<a href="<?php the_sub_field('serv_feat_url'); ?>" target="_self"><img src="<?php the_sub_field('serv_ico'); ?>" alt="<?php the_sub_field('serv_ico_alt'); ?>"></a>
					<h2><?php the_sub_field('service_icon_copy'); ?></h2>
				</div>


			<?php endwhile; ?>
 			<?php endif; ?>
			</div>
		</div>

	</div>
</section>






<!--</body>-->



<?php get_footer(); ?>
